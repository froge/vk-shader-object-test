FSHADERS=$(wildcard src/shaders/*.frag)
VSHADERS=$(wildcard src/shaders/*.vert)

FS = $(patsubst src/shaders/%.frag, release/shaders/%.frag.spv, $(FSHADERS))
VS = $(patsubst src/shaders/%.vert, release/shaders/%.vert.spv, $(VSHADERS))

# frags: $(FSHADERS)
	# glslc $(shell basename $^) -o $^

release/shaders/%.frag.spv: src/shaders/%.frag
	glslc $< -o $@

release/shaders/%.vert.spv: src/shaders/%.vert
	glslc $< -o $@

all: $(FS) $(VS)
	meson compile -C build

run: all
	cp build/vulkan release
	cd release && ./vulkan
