{
    files = {
        "build/.objs/vulkan/linux/x86_64/debug/src/vk_alloc.cpp.o",
        "build/.objs/vulkan/linux/x86_64/debug/src/main.c.o"
    },
    values = {
        "/usr/bin/g++",
        {
            "-m64",
            "-L/usr/lib",
            "-lSDL2",
            "-lvulkan"
        }
    }
}