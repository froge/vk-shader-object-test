{
    depfiles_gcc = "main.o: src/main.c src/common.h\
",
    files = {
        "src/main.c"
    },
    values = {
        "/usr/bin/gcc",
        {
            "-m64",
            "-g",
            "-O0",
            "-I3rdparty",
            "-D_REENTRANT",
            "-isystem",
            "/usr/include/SDL2",
            "-isystem",
            "/home/robin/.xmake/packages/v/vulkan-memory-allocator/v3.0.1/b0edb904e7d94e0ebfac2cec06cdd2c4/include"
        }
    }
}