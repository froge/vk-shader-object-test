#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

#include "SDL_video.h"
#include "vk_mem_alloc.h"
#include "common.h"
#include "stb_image.h"

#define VK_FUNC(name) PFN_ ## name _ ## name = (PFN_ ## name)vkGetInstanceProcAddr(instance, #name)

VmaAllocator allocator;

int check_family_properties(VkPhysicalDevice dev, u32 *fam) {
    u32 count;
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &count, NULL);
    VkQueueFamilyProperties *props = malloc(sizeof(VkQueueFamilyProperties) * count);
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &count, props);
    for (int i = 0; i < count; i++) {
        if (props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            *fam = i;
            return 1;
        }
    }
    return 0;
}

struct ubo0 {
    float time;
};

struct VertexBuffer {
    VmaAllocation allocation;
    VkBuffer buffer;
    VkDeviceSize size;
};

struct VertexBuffer create_vertex_buffer(void *content, VkDeviceSize size) {
    struct VertexBuffer ret = {0};

    ret.size = size;

    VkBufferCreateInfo vbuffer_info = {0};
    vbuffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vbuffer_info.size = size;
    vbuffer_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    vbuffer_info.flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    VmaAllocationCreateInfo alloc_info = {0};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

    vmaCreateBuffer(allocator, &vbuffer_info, &alloc_info, &ret.buffer, &ret.allocation, NULL); 

    float *vert_mapped;
    vmaMapMemory(allocator, ret.allocation, &vert_mapped);

    memcpy(vert_mapped, content, size);

    vmaUnmapMemory(allocator, ret.allocation);

    return ret;
}

int main() {
    char buf[1024];
    getcwd(buf, sizeof(buf));
    strcat(buf, "/layers");
    setenv("VK_ADD_LAYER_PATH", buf, 0);

    puts(getenv("VK_ADD_LAYER_PATH"));
    
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

    SDL_Window *window = SDL_CreateWindow("among us gamer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_VULKAN);
    u32 jort;
    vkEnumerateInstanceVersion(&jort);

    char *extra_exts[] = {"VK_KHR_get_physical_device_properties2"};
    int extra_ext_count = 1;

    u32 exts_count;
    SDL_Vulkan_GetInstanceExtensions(window, &exts_count, NULL);

    char **exts = malloc(sizeof(char*) * exts_count + extra_ext_count);

    SDL_Vulkan_GetInstanceExtensions(window, &exts_count, (const char**)exts);

    for (int i = 0; i < extra_ext_count; i++) {
        exts[i + exts_count] = extra_exts[i];
    }

    exts_count += extra_ext_count;

    for (int i = 0; i < exts_count; i++) {
        printf("%s\n", exts[i]);
    }


    VkApplicationInfo app_info = {0};
    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pApplicationName = "fartnite";
    app_info.applicationVersion = 0;
    app_info.apiVersion = VK_API_VERSION_1_3;
    app_info.pEngineName = "fart_engine";

    const char * layers[] = {"VK_LAYER_KHRONOS_shader_object", "VK_LAYER_KHRONOS_validation"};

    VkInstanceCreateInfo instance_info = {0};
    instance_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instance_info.enabledExtensionCount = exts_count;
    instance_info.ppEnabledExtensionNames = (const char* const*)exts;
    instance_info.enabledLayerCount = 1;
    instance_info.ppEnabledLayerNames = (const char* const*)layers;
    instance_info.pApplicationInfo = &app_info;

    VkInstance instance;
    VkResult res;
    res = vkCreateInstance(&instance_info, NULL, &instance);
    if (res != VK_SUCCESS) {
        printf("instance could not be created: %i\n", res);
        exit(1);
    }

    printf("Vulkan instance version: %i.%i.%i\n", VK_VERSION_MAJOR(jort), VK_VERSION_MINOR(jort), VK_VERSION_PATCH(jort));

    u32 dev_count;
    vkEnumeratePhysicalDevices(instance, &dev_count, NULL);

    VkPhysicalDevice *physdevs = malloc(sizeof(VkPhysicalDevice) * dev_count);

    vkEnumeratePhysicalDevices(instance, &dev_count, physdevs);

    int chosen_dev = 0;
    int chosen_type = -1;
    int chosen_family = -1;

    for (int i = 0; i < dev_count; i++) {
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(physdevs[i], &props);
        u32 fam;
        if (!check_family_properties(physdevs[i], &fam)) {
            continue;
        }
        if (chosen_type == -1) {
            chosen_type = props.deviceType;
            chosen_family = fam;
        } else if (chosen_type != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
            chosen_dev = i;
            chosen_type = props.deviceType;
            chosen_family = fam;
        }
        printf("found %s device %i: %s\n", (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) ? "fast" : "slow", i, props.deviceName);
    }

    if (chosen_family == -1) {
        printf("no compatible device found\n");
        exit(1);
    }

    printf("chose device %i\n", chosen_dev);

    VkDeviceQueueCreateInfo queue_info = {0};
    queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_info.queueFamilyIndex = chosen_family;
    queue_info.queueCount = 1;
    float prio = 1.0;
    queue_info.pQueuePriorities = &prio;

    VkPhysicalDeviceDynamicRenderingFeaturesKHR dynamic_rendering_feature = {0};
    dynamic_rendering_feature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR;
    dynamic_rendering_feature.dynamicRendering = VK_TRUE;
    dynamic_rendering_feature.pNext = NULL;//&shader_object_features;



    VkPhysicalDeviceShaderObjectFeaturesEXT shader_object_features = {0};
    shader_object_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_OBJECT_FEATURES_EXT;
    shader_object_features.shaderObject = VK_TRUE;
    shader_object_features.pNext = &dynamic_rendering_feature;

    VkPhysicalDeviceFeatures2 features2 = {0};
    features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    features2.pNext = &shader_object_features;


    VkDeviceCreateInfo device_info = {0};
    device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    device_info.pNext = &features2;
    // device layers are deprecated
    // no device specific extensions used yet
    device_info.enabledExtensionCount = 7;
    char *ext[7] = {"VK_KHR_maintenance2", "VK_KHR_multiview", "VK_KHR_create_renderpass2", "VK_KHR_depth_stencil_resolve", "VK_KHR_dynamic_rendering", "VK_EXT_shader_object", "VK_KHR_swapchain"};
    // char *ext[1] = {"VK_KHR_swapchain"};
    device_info.ppEnabledExtensionNames = (const char* const*)ext;
    device_info.queueCreateInfoCount = 1;
    device_info.pQueueCreateInfos = &queue_info;

    VkDevice dev;
    res = vkCreateDevice(physdevs[chosen_dev], &device_info, NULL, &dev);
    if (res != VK_SUCCESS) {
        printf("device could not be created: %i\n", res);
        exit(1);
    }

    VkCommandPoolCreateInfo command_pool_info = {0};
    command_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_pool_info.queueFamilyIndex = chosen_family;
    command_pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VkCommandPool pool;
    res = vkCreateCommandPool(dev, &command_pool_info, NULL, &pool);
    if (res != VK_SUCCESS) {
        printf("command pool could not be created: %i\n", res);
        exit(1);
    }

    VkCommandBufferAllocateInfo cmd_info = {0};
    cmd_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmd_info.commandPool = pool;
    cmd_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmd_info.commandBufferCount = 1;

    VkCommandBuffer cmd;
    res = vkAllocateCommandBuffers(dev, &cmd_info, &cmd);
    if (res != VK_SUCCESS) {
        printf("command buffer could not be created: %i\n", res);
        exit(1);
    }

    VkCommandBufferBeginInfo begin_info = {0};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VkSurfaceKHR surf;
    SDL_Vulkan_CreateSurface(window, instance, &surf);

    int w, h;
    SDL_Vulkan_GetDrawableSize(window, &w, &h);

    VkSurfaceCapabilitiesKHR caps;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physdevs[chosen_dev], surf, &caps);

    VkSurfaceFormatKHR format;
    u32 format_count = 1;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physdevs[chosen_dev], surf, &format_count, &format); // TODO choose best format from list

    VkSwapchainCreateInfoKHR swapchain_info = {0};
    swapchain_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchain_info.surface = surf;
    swapchain_info.minImageCount = caps.minImageCount;
    swapchain_info.imageFormat = format.format;
    swapchain_info.imageColorSpace = format.colorSpace;
    swapchain_info.imageExtent = caps.currentExtent;
    swapchain_info.imageArrayLayers = 1;
    swapchain_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    swapchain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchain_info.queueFamilyIndexCount = 1;
    swapchain_info.pQueueFamilyIndices = (u32*)&chosen_family;
    swapchain_info.preTransform = caps.currentTransform;
    swapchain_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchain_info.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    swapchain_info.clipped = VK_TRUE;
    swapchain_info.oldSwapchain = VK_NULL_HANDLE;

    VkSwapchainKHR swapchain;
    res = vkCreateSwapchainKHR(dev, &swapchain_info, NULL, &swapchain);

    u32 chain_count;
    vkGetSwapchainImagesKHR(dev, swapchain, &chain_count, NULL);
    VkImage *swapchain_images = malloc(chain_count * sizeof(VkImage));
    vkGetSwapchainImagesKHR(dev, swapchain, &chain_count, swapchain_images);

    VkImageViewCreateInfo view_info = {0};
    view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    view_info.format = format.format;
    view_info.subresourceRange = (VkImageSubresourceRange) {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1 
    };

    VkImageView *swapchain_image_views = malloc(chain_count * sizeof(VkImageView));

    for (int i = 0; i < chain_count; i++) {
        view_info.image = swapchain_images[i];
        res = vkCreateImageView(dev, &view_info, NULL, &swapchain_image_views[i]);
        if (res != VK_SUCCESS) {
            printf("swapchain image views could not be created: %i\n", res);
            exit(1);
        }
    }

    u32 cur_swap_image = 0;

    int quit = 0;

    VkRenderingAttachmentInfoKHR attachment_info = {0};
    attachment_info.sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO_KHR;
    attachment_info.imageView = swapchain_image_views[cur_swap_image];
    attachment_info.imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachment_info.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment_info.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment_info.resolveMode = VK_RESOLVE_MODE_NONE;
    attachment_info.resolveImageLayout = VK_NULL_HANDLE;
    attachment_info.resolveImageView = VK_NULL_HANDLE;
    // clear value left blank as an exercise for the overly correct (yea.)

    VkRenderingInfoKHR render_info = {0};
    render_info.sType = VK_STRUCTURE_TYPE_RENDERING_INFO_KHR;
    render_info.renderArea = (VkRect2D){(VkOffset2D){0, 0}, caps.currentExtent};
    render_info.layerCount = 1;
    render_info.viewMask = 0;
    render_info.colorAttachmentCount = 1;
    render_info.pColorAttachments = &attachment_info;
    render_info.pDepthAttachment = NULL;
    render_info.pStencilAttachment = NULL;
   
    VkQueue queue;
    vkGetDeviceQueue(dev, chosen_family, 0, &queue);  // TODO

    VkSemaphoreCreateInfo semaphore_info = {0};
    semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fence_info = {0};
    fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    VkSemaphore image_ready;
    VkSemaphore render_done_s;

    vkCreateSemaphore(dev, &semaphore_info, NULL, &image_ready); // TODO
    vkCreateSemaphore(dev, &semaphore_info, NULL, &render_done_s); // TODO

    VkFence render_done;

    vkCreateFence(dev, &fence_info, NULL, &render_done); // TODO

    VkClearColorValue clear_val = {0}; // clear with 0

    VkSubmitInfo submit_info = {0};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &image_ready;
    submit_info.pWaitDstStageMask = (VkPipelineStageFlags[]){ VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &render_done_s;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd;

    VkPresentInfoKHR present_info = {0};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &render_done_s;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &swapchain;
    present_info.pImageIndices = &cur_swap_image;
    present_info.pResults = NULL;

    VkImageMemoryBarrier clear_barrier = {0};
    clear_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    clear_barrier.srcAccessMask = 0;
    clear_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    clear_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    clear_barrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    clear_barrier.srcQueueFamilyIndex = chosen_family;
    clear_barrier.dstQueueFamilyIndex = chosen_family;
    clear_barrier.image = swapchain_images[cur_swap_image];
    clear_barrier.subresourceRange = view_info.subresourceRange;

    VkImageMemoryBarrier present_barrier = clear_barrier;
    present_barrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    present_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    present_barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    present_barrier.dstAccessMask = 0;

    PFN_vkCmdBeginRenderingKHR _vkCmdBeginRenderingKHR = (PFN_vkCmdBeginRenderingKHR)vkGetInstanceProcAddr(instance, "vkCmdBeginRenderingKHR");
    PFN_vkCmdEndRenderingKHR _vkCmdEndRenderingKHR = (PFN_vkCmdEndRenderingKHR)vkGetInstanceProcAddr(instance, "vkCmdEndRenderingKHR");

    FILE *file = fopen("shaders/basic.vert.spv", "rb");
    u64 vlen;
    fseek(file, 0, SEEK_END);
    vlen = ftell(file);
    rewind(file);
    char *vertex_bin = malloc(vlen);
    fread(vertex_bin, vlen, 1, file);
    fclose(file);

    file = fopen("shaders/color.frag.spv", "rb");
    fseek(file, 0, SEEK_END);
    u64 len;
    len = ftell(file);
    rewind(file);
    char *frag_bin = malloc(len);
    fread(frag_bin, len, 1, file);
    fclose(file);

    VkDescriptorSetLayoutBinding bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT, NULL }
    };

    VkDescriptorSetLayoutCreateInfo flayout_info = {0};
    flayout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    flayout_info.bindingCount = 1;
    flayout_info.pBindings = bindings;

    VkDescriptorSetLayout flayout;
    vkCreateDescriptorSetLayout(dev, &flayout_info, NULL, &flayout); 

    VkDescriptorPoolSize pool_size = {0};
    pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    pool_size.descriptorCount = 1;

    VkDescriptorPoolCreateInfo d_pool_info = {0};
    d_pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    d_pool_info.maxSets = 1;
    d_pool_info.poolSizeCount = 1;
    d_pool_info.pPoolSizes = &pool_size;

    VkDescriptorPool descriptor_pool;
    vkCreateDescriptorPool(dev, &d_pool_info, NULL, &descriptor_pool);

    VkBufferCreateInfo buffer_info = {0};
    buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_info.size = sizeof(struct ubo0);
    buffer_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    buffer_info.flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

    VmaAllocatorCreateInfo allocator_create_info = {0};
    allocator_create_info.physicalDevice = physdevs[chosen_dev];
    allocator_create_info.device = dev;
    allocator_create_info.instance = instance;
    allocator_create_info.vulkanApiVersion = VK_API_VERSION_1_3;
    
    vmaCreateAllocator(&allocator_create_info, &allocator);

    VmaAllocationCreateInfo alloc_info = {0};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

    VkBuffer buffer;
    VmaAllocation uniform_allocation;
    VmaAllocationInfo uniform_info;
    vmaCreateBuffer(allocator, &buffer_info, &alloc_info, &buffer, &uniform_allocation, &uniform_info);

    float verts[][2] = {
        {0.5, 0.5},
        {-0.5, 0.5},
        {-0.5, -0.5},
        {0.5, 0.5},
        {-0.5, -0.5},
        {0.5, -0.5},
    };

    struct VertexBuffer vert_buf = create_vertex_buffer(verts, sizeof(verts));

    struct ubo0 *uniform_mapped;
    vmaMapMemory(allocator, uniform_allocation, &uniform_mapped);

    struct ubo0 uniform = {0};
    uniform.time = 1;
    // uniform.color[0] = 1;
    // uniform.color[1] = 0;
    // uniform.color[2] = 0;

    memcpy(uniform_mapped, &uniform, sizeof(struct ubo0));

    VkDescriptorBufferInfo _buffer_info = {0};
    _buffer_info.buffer = buffer;
    _buffer_info.offset = uniform_info.offset;
    _buffer_info.range = VK_WHOLE_SIZE;

    VkDescriptorSetAllocateInfo descriptor_set_info = {0};
    descriptor_set_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_set_info.descriptorPool = descriptor_pool;
    descriptor_set_info.descriptorSetCount = 1;
    descriptor_set_info.pSetLayouts = &flayout;

    VkDescriptorSet descriptor_set;
    vkAllocateDescriptorSets(dev, &descriptor_set_info, &descriptor_set);

    VkWriteDescriptorSet write_ds = {0};
    write_ds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_ds.dstSet = descriptor_set;
    write_ds.dstBinding = 0;
    write_ds.dstArrayElement = 0;
    write_ds.descriptorCount = 1;
    write_ds.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_ds.pBufferInfo = &_buffer_info;

    vkUpdateDescriptorSets(dev, 1, &write_ds, 0, NULL);

    VkPipelineLayoutCreateInfo pipeline_layout_info = {0};
    pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.setLayoutCount = 1;
    pipeline_layout_info.pSetLayouts = &flayout;

    VkPipelineLayout pipeline_layout;
    vkCreatePipelineLayout(dev, &pipeline_layout_info, NULL, &pipeline_layout);

    VkShaderCreateInfoEXT shader_infos[2] = {
        {
            .sType = VK_STRUCTURE_TYPE_SHADER_CREATE_INFO_EXT,
            .pNext = NULL,
            .flags = VK_SHADER_CREATE_LINK_STAGE_BIT_EXT,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .nextStage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .codeType = VK_SHADER_CODE_TYPE_SPIRV_EXT,
            .codeSize = vlen,
            .pCode = vertex_bin,
            .pName = "main",
            .setLayoutCount = 1,
            .pSetLayouts = &flayout,// THESE HAVE TO BE THE SAME WHEN I PLAN ON LINKING THEM!!!!!!!!
            .pushConstantRangeCount = 0,
            .pPushConstantRanges = NULL,
            .pSpecializationInfo = NULL
        },
        {

            .sType = VK_STRUCTURE_TYPE_SHADER_CREATE_INFO_EXT,
            .pNext = NULL,
            .flags = VK_SHADER_CREATE_LINK_STAGE_BIT_EXT,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .nextStage = 0,
            .codeType = VK_SHADER_CODE_TYPE_SPIRV_EXT,
            .codeSize = len,
            .pCode = frag_bin,
            .pName = "main",
            .setLayoutCount = 1,
            .pSetLayouts = &flayout,
            .pushConstantRangeCount = 0,
            .pPushConstantRanges = NULL,
            .pSpecializationInfo = NULL
        }
    };

    VK_FUNC(vkCreateShadersEXT);
    VK_FUNC(vkCmdBindShadersEXT);
    VK_FUNC(vkCmdSetPolygonModeEXT);
    VK_FUNC(vkCmdSetVertexInputEXT);
    VK_FUNC(vkCmdSetColorWriteMaskEXT);
    VK_FUNC(vkCmdSetColorBlendEnableEXT);
    VK_FUNC(vkCmdSetDepthClampEnableEXT);
    VK_FUNC(vkCmdSetRasterizationSamplesEXT);
    VK_FUNC(vkCmdSetSampleMaskEXT);
    VK_FUNC(vkCmdSetDepthBiasEnableEXT);
    VK_FUNC(vkCmdSetLogicOpEnableEXT);

    VkShaderEXT shaders[2];
    res = _vkCreateShadersEXT(dev, 2, shader_infos, NULL, shaders);
    if (res != VK_SUCCESS) {
        printf("shaders could not be bound: %i\n", res);
        exit(1);
    }

    VkShaderStageFlagBits stages[2] = {
        VK_SHADER_STAGE_VERTEX_BIT,
        VK_SHADER_STAGE_FRAGMENT_BIT,
    };
          
    SDL_Event event;

    while (!quit) {
    // {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }
        
        uniform.time = SDL_GetTicks() / 1000.0 / 4.0;
        vkWaitForFences(dev, 1, &render_done, VK_TRUE, (i64)-1);
        vkResetFences(dev, 1, &render_done);
        vkAcquireNextImageKHR(dev, swapchain, (i64)-1, image_ready, VK_NULL_HANDLE, &cur_swap_image);

        ///
        // uniform.color[0] = 1;
        // uniform.color[1] = 0;
        // uniform.color[2] = 0;

        memcpy(uniform_mapped, &uniform, sizeof(struct ubo0));

        vkUpdateDescriptorSets(dev, 1, &write_ds, 0, NULL);
        ///

        attachment_info.imageView = swapchain_image_views[cur_swap_image];
        render_info.pColorAttachments = &attachment_info;

        clear_barrier.image = swapchain_images[cur_swap_image];
        present_barrier.image = swapchain_images[cur_swap_image];

        res = vkBeginCommandBuffer(cmd, &begin_info);
        if (res != VK_SUCCESS) {
            printf("command buffer could not begin: %i\n", res);
            exit(1);
        }

        vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0, 0, NULL, 0, NULL, 1, &clear_barrier);
    
        _vkCmdBeginRenderingKHR(cmd, &render_info);

        VkViewport viewport = {0};
        viewport.x = 0;
        viewport.y = 0;
        viewport.width = caps.currentExtent.width;
        viewport.height = caps.currentExtent.height;

        VkRect2D scissor = {0};
        scissor.offset = (VkOffset2D){0, 0};
        scissor.extent = caps.currentExtent;

        VkVertexInputBindingDescription2EXT input_binding = {0};
        input_binding.sType = VK_STRUCTURE_TYPE_VERTEX_INPUT_BINDING_DESCRIPTION_2_EXT;
        input_binding.binding = 0;
        input_binding.stride = sizeof(float[2]);
        input_binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        input_binding.divisor = 1;

        VkVertexInputAttributeDescription2EXT input_attribute = {0};
        input_attribute.sType = VK_STRUCTURE_TYPE_VERTEX_INPUT_ATTRIBUTE_DESCRIPTION_2_EXT;
        input_attribute.location = 0;
        input_attribute.binding = 0;
        input_attribute.format = VK_FORMAT_R32G32_SFLOAT;
        input_attribute.offset = 0;

        _vkCmdSetVertexInputEXT(cmd, 1, &input_binding, 1, &input_attribute);
        vkCmdSetViewport(cmd, 0, 1, &viewport);
        vkCmdSetScissor(cmd, 0, 1, &scissor);

        vkCmdSetRasterizerDiscardEnable(cmd, VK_FALSE);

        vkCmdSetPrimitiveTopology(cmd, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
        vkCmdSetPrimitiveRestartEnable(cmd, VK_FALSE);
        _vkCmdSetRasterizationSamplesEXT(cmd, VK_SAMPLE_COUNT_1_BIT);

        VkSampleMask sample_mask = 0x1;
        _vkCmdSetSampleMaskEXT(cmd, VK_SAMPLE_COUNT_1_BIT, &sample_mask);

        _vkCmdSetPolygonModeEXT(cmd, VK_POLYGON_MODE_FILL);
        
        vkCmdSetFrontFace(cmd, VK_FRONT_FACE_CLOCKWISE);

        vkCmdSetDepthTestEnable(cmd, VK_FALSE);
        vkCmdSetDepthCompareOp(cmd, VK_COMPARE_OP_LESS);
        vkCmdSetDepthBoundsTestEnable(cmd, VK_FALSE);
        _vkCmdSetDepthBiasEnableEXT(cmd, VK_FALSE);

        vkCmdSetStencilTestEnable(cmd, VK_FALSE);
        _vkCmdSetLogicOpEnableEXT(cmd, VK_FALSE);

        _vkCmdSetColorBlendEnableEXT(cmd, 0, 1, &(VkBool32){VK_FALSE});

        VkColorComponentFlags test1 = VK_COLOR_COMPONENT_A_BIT | VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;
        _vkCmdSetColorWriteMaskEXT(cmd, 0, 1, &test1);

        vkCmdSetCullMode(cmd, VK_CULL_MODE_BACK_BIT);
        vkCmdSetDepthWriteEnable(cmd, VK_FALSE);

        _vkCmdSetDepthClampEnableEXT(cmd, VK_FALSE);


        VkDeviceSize offsets[] = { 0 };
        VkDeviceSize sizes[] = { sizeof(verts) };
        VkDeviceSize strides[] = { sizeof(float[2]) };
        vkCmdBindVertexBuffers2(cmd, 0, 1, &vert_buf.buffer, offsets, sizes, strides);
        //_vkCmdSetVertexInputEXT(cmd, 0, NULL, 0, NULL);

        _vkCmdBindShadersEXT(cmd, 2, stages, shaders);
        _vkCmdBindShadersEXT(cmd, 1, &(VkShaderStageFlagBits){VK_SHADER_STAGE_GEOMETRY_BIT}, NULL);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout, 0, 1, &descriptor_set, 0, NULL);

        vkCmdDraw(cmd, 6, 1, 0, 0);

        _vkCmdEndRenderingKHR(cmd);

        vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1, &present_barrier);

        vkEndCommandBuffer(cmd);
        vkQueueSubmit(queue, 1, &submit_info, render_done);
        vkQueuePresentKHR(queue, &present_info);
    }

    vkQueueWaitIdle(queue);

    for (int i = 0; i < chain_count; i++) {
        vkDestroyImageView(dev, swapchain_image_views[i], NULL);
    }
    vkDestroySemaphore(dev, image_ready, NULL);
    vkDestroySemaphore(dev, render_done_s, NULL);
    vkWaitForFences(dev, 1, &render_done, VK_TRUE, (i64)-1);
    vkResetFences(dev, 1, &render_done);
    vkDestroyFence(dev, render_done, NULL);
    vkDestroySwapchainKHR(dev, swapchain, NULL);
    vkDestroySurfaceKHR(instance, surf, NULL);
    vkFreeCommandBuffers(dev, pool, 1, &cmd);
    vkDestroyCommandPool(dev, pool, NULL);
    vkDestroyDevice(dev, NULL);
    vkDestroyInstance(instance, NULL);
}
