{
    files = {
        "./.objs/vulkan/linux/x86_64/release/src/main.c.o"
    },
    values = {
        "/usr/bin/g++",
        {
            "-m64",
            "-L/usr/lib",
            "-s",
            "-lSDL2",
            "-lvulkan"
        }
    }
}