{
    files = {
        "src/main.c"
    },
    depfiles_gcc = "main.o: src/main.c src/common.h\
",
    values = {
        "/usr/bin/gcc",
        {
            "-m64",
            "-fvisibility=hidden",
            "-O3",
            "-D_REENTRANT",
            "-isystem",
            "/usr/include/SDL2",
            "-isystem",
            "/home/robin/.xmake/packages/v/vulkan-memory-allocator/v3.0.1/b0edb904e7d94e0ebfac2cec06cdd2c4/include",
            "-DNDEBUG"
        }
    }
}